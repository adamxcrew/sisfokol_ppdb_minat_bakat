<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();

require("../inc/config.php");
require("../inc/fungsi.php");
require("../inc/koneksi.php");
require("../inc/cek/siswa.php");
require("../inc/class/paging.php");
$tpl = LoadTpl("../template/siswa.html");

nocache;

//nilai
$filenya = "soal.php";
$judul = "[SOAL YANG DIKERJAKAN]...";
$judulku = "$judul";
$judulx = $judul;
$jkd = nosql($_REQUEST['jkd']);
$kd = nosql($_REQUEST['kd']);
$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}



$sesiku = $kd61_session;







//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///ke index
if ($_POST['btnDF'])
	{
	//re-direct
	$ke = "index.php";
	xloc($ke);
	exit();
	}




///loncat ke soal lainnya...
if ($_POST['btnLAGI'])
	{
	//re-direct
	$ke = "$filenya?jkd=$jkd";
	xloc($ke);
	exit();
	}





//jika selesai
if ($_POST['btnSELESAI'])
	{
	//ambil nilai
	$jkd = nosql($_POST['jkd']);
	
	//update
	mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET revisi = 'false' ".
					"WHERE jadwal_kd = '$jkd' ".
					"AND siswa_kd = '$sesiku'");
	
	
	
	//re-direct
	$ke = "$filenya?jkd=$jkd";
	xloc($ke);
	exit();
	}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//isi *START
ob_start();


//require
require("../template/js/jumpmenu.js");
require("../template/js/swap.js");
?>



  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo '<form action="'.$filenya.'" method="post" name="formxx"><p>
<input name="jkd" type="hidden" value="'.$jkd.'">
<input name="btnDF" type="submit" value="KEMBALI KE BERANDA >" class="btn btn-danger">
</p>
<br>

</form>';





$limit = 50;




//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>


  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../template/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../template/adminlte/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="../template/adminlte/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../template/adminlte/dist/css/skins/skins-biasawae.css">
	
	
	
	


  
	  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//detail jkd jadwal
$qku = mysqli_query($koneksi, "SELECT * FROM m_jadwal ".
						"WHERE kd = '$jkd' LIMIT 0,1");
$rku = mysqli_fetch_assoc($qku);
$u_durasi = balikin($rku['durasi']);
$u_mapel = balikin($rku['mapel']);



//jumlah soal
$qjml = mysqli_query($koneksi, "SELECT kd FROM m_soal ".
						"WHERE jadwal_kd = '$jkd' ".
						"ORDER BY round(no) ASC");
$tjml = mysqli_num_rows($qjml);	
	
	
	

//yg dikerjakan...
$qyuk = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
						"WHERE siswa_kd = '$sesiku' ".
						"AND jadwal_kd = '$jkd' ".
						"AND jawab <> ''");
$ryuk = mysqli_fetch_assoc($qyuk);
$yuk_dikerjakan = mysqli_num_rows($qyuk);


//jika lebih, itu tjml
if ($yuk_dikerjakan > $tjml)
	{
	$yuk_dikerjakan = $tjml;
	}

?>


<script language='javascript'>
//membuat document jquery
$(document).ready(function(){

		$.ajax({
			url: "<?php echo $sumber;?>/calon/i_timer.php?aksi=sisawaktu&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>",
			type:$(this).attr("method"),
			data:$(this).serialize(),
			success:function(data){					
				$("#sisawaktu").html(data);
				}
			});
			
			






		$.ajax({
			url: "<?php echo $sumber;?>/calon/i_timer.php?aksi=setpostdate&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>",
			type:$(this).attr("method"),
			data:$(this).serialize(),
			success:function(data){					
				$("#setpostdate").html(data);
				}
			});
			
			





		
		setInterval(poll,5000);
		
		function poll()
			{
			$.ajax({
				url: "<?php echo $sumber;?>/calon/i_jawabku.php?aksi=form&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>",
				type:$(this).attr("method"),
				data:$(this).serialize(),
				success:function(data){					
					$("#jawabanku").html(data);
					}
				});
			}
		


		
});

</script>


      <div class="row">

        <!-- /.col -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><?php echo $u_mapel;?> [<?php echo $tjml;?> Soal]</span>
              <span class="info-box-number">
              
				<?php
				echo '<p>
				[<b>'.$u_durasi.' Menit</b>].
				</p>';
				?>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->



        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="glyphicon glyphicon-education"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Telah Dikerjakan</span>
              <span class="info-box-number">
              <div id="udahjawab">
              	<b><?php echo $yuk_dikerjakan;?></b>
				</div>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->




        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="glyphicon glyphicon-time"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sisa Waktu</span>
              <span class="info-box-number">
              <div id="sisawaktu"></div>
              <div id="setpostdate"></div>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        




       </div>
      <!-- /.row -->


              
				
<?php



echo '</form>
<hr>';




	
//jml soal yg ada
$qyuk7 = mysqli_query($koneksi, "SELECT kd FROM m_soal ".
						"WHERE jadwal_kd = '$jkd'");
$ryuk7 = mysqli_fetch_assoc($qyuk7);
$tyuk7 = mysqli_num_rows($qyuk7);




//yg dijawab
$qyuk8 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
						"WHERE siswa_kd = '$sesiku' ".
						"AND jadwal_kd = '$jkd' ".
						"AND jawab <> ''");
$ryuk8 = mysqli_fetch_assoc($qyuk8);
$tyuk8 = mysqli_num_rows($qyuk8);






//yg dijawab
$xyzz = md5("$jkd$sesiku");


//insert
mysqli_query($koneksi, "INSERT INTO siswa_soal_nilai(kd, siswa_kd, jadwal_kd, waktu_mulai, postdate) VALUES ".
				"('$xyzz', '$sesiku', '$jkd', '$today', '$today')");

					

					
					
					
					
//cek, ada set revisi atau tidak ya ////////////////////////////////////////////////////////////////////
$qyuk11 = mysqli_query($koneksi, "SELECT * FROM siswa_soal_nilai ".
						"WHERE siswa_kd = '$sesiku' ".
						"AND jadwal_kd = '$jkd' ".
						"AND revisi = 'true'");
$ryuk11 = mysqli_fetch_assoc($qyuk11);
$tyuk11 = mysqli_num_rows($qyuk11);
		






//jika udah semua... ///////////////////////////////////////////////////////////////////////////////////
if ($tyuk7 <= $tyuk8)
	{
	//hitung yg benar
	$qyuk2 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd' ".
							"AND benar = 'true'");
	$ryuk2 = mysqli_fetch_assoc($qyuk2);
	$jml_benar = mysqli_num_rows($qyuk2);
	$jml_salah = $tyuk7 - $jml_benar; 



	
	

	//update
	mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET jml_benar = '$jml_benar', ".
					"jml_salah = '$jml_salah', ".
					"postdate = '$today' ".
					"WHERE siswa_kd = '$sesiku' ".
					"AND jadwal_kd = '$jkd'");
					
					
					
	//jika memang selesai
	if (empty($tyuk11))
		{
		?>
	
	
	
	        <!-- /.col -->
	        <div class="col-md-12 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-duplicate"></i></span>
	
	            <div class="info-box-content">
	              <span class="info-box-text">Rekap Jawaban</span>
	              <span class="info-box-number">
	              [Benar : <font color="green"><?php echo $jml_benar;?></font>].
	              [Salah : <font color="red"><?php echo $jml_salah;?></font>]. 
	
	              </span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	        </div>
	        <!-- /.col -->
	        
	        
		<?php
		}
		
		
	else
		{
		//masih ada yg perlu revisi jawaban ///////////////////////////////////////////////////////////
			?>
		
		
		
		
			<style>
			
			
			#myfooter{
			   position: fixed;
			   left: 0;
			   bottom: 0;
			  height: 6em;
			  background-color: #f5f5f5;
			  text-align: center;
			   width: 100%;
			   color: green;;
			
			}
			
			
			
			
			</style>
			
		
		
		
		
			   <!-- /.col -->
		        <div class="col-md-12 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-pushpin"></i></span>
		
		            <div class="info-box-content">
		              <span class="info-box-text">PERHATIAN</span>
		              <span class="info-box-number">
		              Silahkan Kerjakan dengan Baik. Terima Kasih.  
		
		              </span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		        
		
		
		
			<div id="myfooter">
		
			   <!-- /.col -->
		        <div class="col-md-12 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <div class="info-box-content">
		              <span class="info-box-text">DIJAWAB</span>
		              <span class="info-box-number">
		              
		              <div id="jawabanku"></div>  
		
		              </span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		       	 
			</div>
		
			<?php
			//cek dulu... udah ada atau belum
			$qyuk2 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
									"WHERE siswa_kd = '$sesiku' ".
									"AND jadwal_kd = '$jkd' LIMIT 0,1");
			$ryuk2 = mysqli_fetch_assoc($qyuk2);
			$tyuk2 = mysqli_num_rows($qyuk2);
			
			
			//jika null, kasi semua dulu..
			if (empty($tyuk2))
				{
				$qkuy = mysqli_query($koneksi, "SELECT * FROM m_soal ".
												"WHERE jadwal_kd = '$jkd' ".
												"ORDER BY postdate DESC");
				$rkuy = mysqli_fetch_assoc($qkuy);
				
				do
					{
					//nilai
					$kuy_no = $kuy_no + 1;
					$kuy_rand = rand(10, 100);
					$kuy_kd = nosql($rkuy['kd']);
					$kuy_kunci = balikin($rkuy['kunci']);
					//$xyz = md5("$kuy_rand$sesiku$kuy_kd$jkd$kuy_no");
					$xyz1 = md5("$sesiku$jkd$kuy_kd$kuy_no");
					$xyz = "$xyz1$kuy_rand";
				
												
					//insert
					mysqli_query($koneksi, "INSERT INTO siswa_soal(kd, jadwal_kd, ".
									"siswa_kd, soal_kd, jawab, ".
									"kunci, benar, postdate) VALUES ".
									"('$xyz', '$jkd', ".
									"'$sesiku', '$kuy_kd', '', ".
									"'$kuy_kunci', 'false', '$today')");
					}
				while ($rkuy = mysqli_fetch_assoc($qkuy));
				}
			
			
			
			
			
			
			
			
			//ambil sesi postdate revisi
			$postdate_rev = balikin($_SESSION['revisi_postdate']);
						
			
		
		
		
			//query batas akhir
			$qmboh2 = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
						"WHERE jadwal_kd = '$jkd' ".
						"AND siswa_kd = '$sesiku' ".
						"AND jawab <> '' ".
						"ORDER BY postdate DESC LIMIT 0,1");
			$rmboh2 = mysqli_fetch_assoc($qmboh2);
			$tmboh2 = mysqli_num_rows($qmboh2);
			$mboh2_postdate_akhir = balikin($rmboh2['postdate']);
			$i_kdx = nosql($rmboh2['soal_kd']);
			
			if ($postdate_rev == $mboh2_postdate_akhir)
				{
				//query
				$qmboh = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
							"WHERE jadwal_kd = '$jkd' ".
							"AND siswa_kd = '$sesiku' ".
							"AND jawab <> '' ".
							"ORDER BY postdate ASC LIMIT 0,1");
				$rmboh = mysqli_fetch_assoc($qmboh);
				$tmboh = mysqli_num_rows($qmboh);
				$mboh_postdate = balikin($rmboh['postdate']);
				$i_kdx = nosql($rmboh['soal_kd']);
				}
			
			else
				{
				//query
				$qmboh = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
							"WHERE jadwal_kd = '$jkd' ".
							"AND siswa_kd = '$sesiku' ".
							"AND jawab <> '' ".
							"AND postdate > '$postdate_rev' ".
							"ORDER BY postdate ASC LIMIT 0,1");
				$rmboh = mysqli_fetch_assoc($qmboh);
				$tmboh = mysqli_num_rows($qmboh);
				$mboh_postdate = balikin($rmboh['postdate']);
				$i_kdx = nosql($rmboh['soal_kd']);
				}
	
			
			
			//jika null
			if (empty($tmboh))
				{
				//query
				$qmboh = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
							"WHERE jadwal_kd = '$jkd' ".
							"AND siswa_kd = '$sesiku' ".
							"AND jawab <> '' ".
							"ORDER BY postdate ASC LIMIT 0,1");
				$rmboh = mysqli_fetch_assoc($qmboh);
				$tmboh = mysqli_num_rows($qmboh);
				$mboh_postdate = balikin($rmboh['postdate']);
				$i_kdx = nosql($rmboh['soal_kd']);
				}
				
				
				
			//bikin sesi
			$_SESSION['revisi_postdate'] = $mboh_postdate;
							
			$postdate_rev = balikin($_SESSION['revisi_postdate']);
			
			
			echo "&nbsp;";
			
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
			$nomer = $nomer + 1;
			
			
	
			$i_kdx = nosql($rmboh['soal_kd']);
			
			//detail e
			$qkuy = mysqli_query($koneksi, "SELECT * FROM m_soal ".
											"WHERE jadwal_kd = '$jkd' ".
											"AND kd = '$i_kdx'");
			$rkuy = mysqli_fetch_assoc($qkuy);						
			$i_kd = nosql($rkuy['kd']);
			$i_no = balikin($rkuy['no']);
			$i_kunci = balikin($rkuy['kunci']);
			$i_isi = balikin($rkuy['isi']);
			$i_postdate = balikin($rkuy['postdate']);
	
			
			//yg dijawab
			$qyuk = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
									"WHERE siswa_kd = '$sesiku' ".
									"AND jadwal_kd = '$jkd' ".
									"AND soal_kd = '$i_kd'");
			$ryuk = mysqli_fetch_assoc($qyuk);
			$yuk_kdku = nosql($ryuk['kd']);
			$yuk_jawabku = balikin($ryuk['jawab']);
			
			
			
			?>
				<script language='javascript'>
			//membuat document jquery
			$(document).ready(function(){
							
							
							
				$('#xpilih<?php echo $nomer;?>').change(function() {
					var nilku = $(this).val();
	
					$('#iproses<?php echo $i_kd;?>').show();
							
					
					$.ajax({
						url: "i_jawab.php?aksi=simpan&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>&soalkd=<?php echo $i_kd;?>&nilku="+nilku,
						type:$(this).attr("method"),
						data:$(this).serialize(),
						success:function(data){				
							$("#ihasil<?php echo $nomer;?>").html(data);
							$('#iproses<?php echo $i_kd;?>').hide();
							}
						});
					
					
					
					
					$.ajax({
						url: "i_jawab.php?aksi=hitung&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>&soalkd=<?php echo $i_kd;?>&nilku="+nilku,
						type:$(this).attr("method"),
						data:$(this).serialize(),
						success:function(data){					
							$("#udahjawab").html(data);
							}
						});
					
	
					
					$.ajax({
						url: "i_timer.php?aksi=setpostdate&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>",
						type:$(this).attr("method"),
						data:$(this).serialize(),
						success:function(data){					
							$("#setpostdate").html(data);
							}
						});
						
					
			    });
	
	
					
			});
			
			</script>
	
	
	
	
			<?php
	
			echo '<a name="ku'.$i_kd.'"></a>
			Dikerjakan pada : <b>'.$mboh_postdate.'</b>
			<div class="table-responsive">          
			<table class="table" border="1">
			<thead>
			<tr valign="top" bgcolor="'.$e_warnaheader.'">
			<td><strong><font color="'.$e_warnatext.'">SOAL</font></strong></td>
			</tr>
			</thead>
			<tbody>';
					
			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$e_warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>
			'.$i_isi.'
			
			<hr>
			
			<p>
			 
			<form name="xformx'.$nomer.'" id="xformx'.$nomer.'">
			
			Jawab : <select name="xpilih'.$nomer.'" id="xpilih'.$nomer.'" class="btn btn-warning">
						<option value="'.$yuk_jawabku.'" selected>'.$yuk_jawabku.'</option>	
						<option value="A">A</option>	
						<option value="B">B</option>	
						<option value="C">C</option>	
						<option value="D">D</option>	
						<option value="E">E</option>	
						</select>			
			
			</p>		
			</form>
					
			<div id="iproses'.$i_kd.'" style="display:none">
				<img src="'.$sumber.'/template/img/progress-bar.gif" width="100" height="16">
			</div>
			
			<div id="ihasil'.$nomer.'"></div>
			
			
			</td>
	        </tr>
			</tbody>
		  	</table>
		  	</div>';
						
			?>
			
			<script language='javascript'>
			//membuat document jquery
			$(document).ready(function(){
				
				$("#btnLAGI").on('click', function(){
				
			    	window.location.href = "<?php echo $filenya;?>?jkd=<?php echo $jkd;?>";
				
				});	
		
		
				
				$("#btnSELESAI").on('click', function(){
				
			    	window.location.href = "<?php echo $filenya;?>?jkd=<?php echo $jkd;?>&s=selesai";
				
				});	
		
					
			});
			
			</script>
		
		
			<?php
				
			
			echo '<br>
			<div id="ihasilselesai"></div>
			<div id="iprosesku" style="display:none">
				<img src="'.$sumber.'/template/img/progress-bar.gif" width="100" height="16">
			</div>
		
		
			<form action="'.$filenya.'" method="post" name="formxx2">
			<hr>
			<input name="jkd" type="hidden" value="'.$jkd.'">
			<input name="btnLAGI" id="btnLAGI" type="submit" class="btn btn-success" value="LIHAT SOAL LAINNYA >>">
			<input name="btnSELESAI" id="btnSELESAI" type="submit" class="btn btn-danger" value="SELESAI. KUMPULKAN >>">
			<hr>
			<br>
			
			</form>
			
			<br>
			<br>
			<br>';
		
		
		
		
		
		
			
			
			
		
			
			
			//jml soal yg ada
			$qyuk7 = mysqli_query($koneksi, "SELECT kd FROM m_soal ".
									"WHERE jadwal_kd = '$jkd'");
			$ryuk7 = mysqli_fetch_assoc($qyuk7);
			$tyuk7 = mysqli_num_rows($qyuk7);
		
		
			
			//hitung yg benar
			$qyuk2 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
									"WHERE siswa_kd = '$sesiku' ".
									"AND jadwal_kd = '$jkd' ".
									"AND benar = 'true'");
			$ryuk2 = mysqli_fetch_assoc($qyuk2);
			$jml_benar = mysqli_num_rows($qyuk2);
			$jml_salah = $tyuk7 - $jml_benar; 
			$xyzz = md5("$jkd$sesiku");
		
		
							
		
			//update
			mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET jml_benar = '$jml_benar', ".
							"jml_salah = '$jml_salah', ".
							"postdate = '$today' ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd'");
							
							
							
							
			//hitung jumlah yg dikerjakan
			$qyuk = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
									"WHERE siswa_kd = '$sesiku' ".
									"AND jadwal_kd = '$jkd' ".
									"AND jawab <> ''");
			$ryuk = mysqli_fetch_assoc($qyuk);
			$tyuk = mysqli_num_rows($qyuk);
			
			
		
		
			//update nilai
			mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET waktu_selesai = '$today', ".
							"jml_soal_dikerjakan = '$tyuk', ".
							"jml_benar = '$jml_benar', ".
							"jml_salah = '$jml_salah' ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd'");
						

		
				
		}
	}



else		

	{
	
	?>




	<style>
	
	
	#myfooter{
	   position: fixed;
	   left: 0;
	   bottom: 0;
	  height: 6em;
	  background-color: #f5f5f5;
	  text-align: center;
	   width: 100%;
	   color: green;;
	
	}
	
	
	
	
	</style>
	




	   <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-pushpin"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PERHATIAN</span>
              <span class="info-box-number">
              Silahkan Kerjakan dengan Baik. Terima Kasih.  

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        



	<div id="myfooter">

	   <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box">
            <div class="info-box-content">
              <span class="info-box-text">DIJAWAB</span>
              <span class="info-box-number">
              
              <div id="jawabanku"></div>  

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
       	 
	</div>

	<?php
	//cek dulu... udah ada atau belum
	$qyuk2 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd' LIMIT 0,1");
	$ryuk2 = mysqli_fetch_assoc($qyuk2);
	$tyuk2 = mysqli_num_rows($qyuk2);
	
	
	//jika null, kasi semua dulu..
	if (empty($tyuk2))
		{
		$qkuy = mysqli_query($koneksi, "SELECT * FROM m_soal ".
										"WHERE jadwal_kd = '$jkd' ".
										"ORDER BY postdate DESC");
		$rkuy = mysqli_fetch_assoc($qkuy);
		
		do
			{
			//nilai
			$kuy_no = $kuy_no + 1;
			$kuy_rand = rand(10, 100);
			$kuy_kd = nosql($rkuy['kd']);
			$kuy_kunci = balikin($rkuy['kunci']);
			//$xyz = md5("$kuy_rand$sesiku$kuy_kd$jkd$kuy_no");
			$xyz1 = md5("$sesiku$jkd$kuy_kd$kuy_no");
			$xyz = "$xyz1$kuy_rand";
		
										
			//insert
			mysqli_query($koneksi, "INSERT INTO siswa_soal(kd, jadwal_kd, ".
							"siswa_kd, soal_kd, jawab, ".
							"kunci, benar, postdate) VALUES ".
							"('$xyz', '$jkd', ".
							"'$sesiku', '$kuy_kd', '', ".
							"'$kuy_kunci', 'false', '$today')");
			}
		while ($rkuy = mysqli_fetch_assoc($qkuy));
		}
	
	
	
	
	
	
	
	
	

	
	//query
	$qmboh = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
				"WHERE jadwal_kd = '$jkd' ".
				"AND siswa_kd = '$sesiku' ".
				"AND jawab = '' ".
				"ORDER BY RAND() LIMIT 0,1");
	$rmboh = mysqli_fetch_assoc($qmboh);
	$tmboh = mysqli_num_rows($qmboh);
	
	
	
	//jika ada
	if (!empty($tmboh))
		{
		echo "&nbsp;";
		
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		$nomer = $nomer + 1;
		
		

		$i_kdx = nosql($rmboh['soal_kd']);
		
		//detail e
		$qkuy = mysqli_query($koneksi, "SELECT * FROM m_soal ".
										"WHERE jadwal_kd = '$jkd' ".
										"AND kd = '$i_kdx'");
		$rkuy = mysqli_fetch_assoc($qkuy);						
		$i_kd = nosql($rkuy['kd']);
		$i_no = balikin($rkuy['no']);
		$i_kunci = balikin($rkuy['kunci']);
		$i_isi = balikin($rkuy['isi']);
		$i_postdate = balikin($rkuy['postdate']);

		
		//yg dijawab
		$qyuk = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
								"WHERE siswa_kd = '$sesiku' ".
								"AND jadwal_kd = '$jkd' ".
								"AND soal_kd = '$i_kd'");
		$ryuk = mysqli_fetch_assoc($qyuk);
		$yuk_kdku = nosql($ryuk['kd']);
		$yuk_jawabku = balikin($ryuk['jawab']);
		
		
		
		?>
			<script language='javascript'>
		//membuat document jquery
		$(document).ready(function(){
						
						
						
			$('#xpilih<?php echo $nomer;?>').change(function() {
				var nilku = $(this).val();

				$('#iproses<?php echo $i_kd;?>').show();
						
				
				$.ajax({
					url: "i_jawab.php?aksi=simpan&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>&soalkd=<?php echo $i_kd;?>&nilku="+nilku,
					type:$(this).attr("method"),
					data:$(this).serialize(),
					success:function(data){				
						$("#ihasil<?php echo $nomer;?>").html(data);
						$('#iproses<?php echo $i_kd;?>').hide();
						}
					});
				
				
				
				
				$.ajax({
					url: "i_jawab.php?aksi=hitung&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>&soalkd=<?php echo $i_kd;?>&nilku="+nilku,
					type:$(this).attr("method"),
					data:$(this).serialize(),
					success:function(data){					
						$("#udahjawab").html(data);
						}
					});
				

				
				$.ajax({
					url: "i_timer.php?aksi=setpostdate&jkd=<?php echo $jkd;?>&skd=<?php echo $sesiku;?>",
					type:$(this).attr("method"),
					data:$(this).serialize(),
					success:function(data){					
						$("#setpostdate").html(data);
						}
					});
					
				
		    });


				
		});
		
		</script>




		<?php

		echo '<a name="ku'.$i_kd.'"></a>
		
		<div class="table-responsive">          
		<table class="table" border="1">
		<thead>
		<tr valign="top" bgcolor="'.$e_warnaheader.'">
		<td><strong><font color="'.$e_warnatext.'">SOAL</font></strong></td>
		</tr>
		</thead>
		<tbody>';
				
		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$e_warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>
		'.$i_isi.'
		
		<hr>
		
		<p>
		 
		<form name="xformx'.$nomer.'" id="xformx'.$nomer.'">
		
		Jawab : <select name="xpilih'.$nomer.'" id="xpilih'.$nomer.'" class="btn btn-warning">
					<option value="'.$yuk_jawabku.'" selected>'.$yuk_jawabku.'</option>	
					<option value="A">A</option>	
					<option value="B">B</option>	
					<option value="C">C</option>	
					<option value="D">D</option>	
					<option value="E">E</option>	
					</select>			
		
		</p>		
		</form>
				
		<div id="iproses'.$i_kd.'" style="display:none">
			<img src="'.$sumber.'/template/img/progress-bar.gif" width="100" height="16">
		</div>
		
		<div id="ihasil'.$nomer.'"></div>
		
		
		</td>
        </tr>
		</tbody>
	  	</table>
	  	</div>';
		}

	else
		{
		?>


        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SEMUA SOAL</span>
              <span class="info-box-number">Sudah Dikerjakan</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        


		<?php
		}						



				
	?>
	
	<script language='javascript'>
	//membuat document jquery
	$(document).ready(function(){
		
		$("#btnLAGI").on('click', function(){
		
	    	window.location.href = "<?php echo $filenya;?>?jkd=<?php echo $jkd;?>";
		
		});	


			
	});
	
	</script>


	<?php
		
	
	echo '<br>
	<div id="ihasilselesai"></div>
	<div id="iprosesku" style="display:none">
		<img src="'.$sumber.'/template/img/progress-bar.gif" width="100" height="16">
	</div>


	<form action="'.$filenya.'" method="post" name="formxx2">
	<hr>
	<input name="jkd" type="hidden" value="'.$jkd.'">
	<input name="btnLAGI" id="btnLAGI" type="submit" class="btn btn-block btn-success" value="LIHAT SOAL LAINNYA >>">
	<hr>
	<br>
	
	</form>
	
	<br>
	<br>
	<br>';






	
	
	

	
	
	//jml soal yg ada
	$qyuk7 = mysqli_query($koneksi, "SELECT kd FROM m_soal ".
							"WHERE jadwal_kd = '$jkd'");
	$ryuk7 = mysqli_fetch_assoc($qyuk7);
	$tyuk7 = mysqli_num_rows($qyuk7);


	
	//hitung yg benar
	$qyuk2 = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd' ".
							"AND benar = 'true'");
	$ryuk2 = mysqli_fetch_assoc($qyuk2);
	$jml_benar = mysqli_num_rows($qyuk2);
	$jml_salah = $tyuk7 - $jml_benar; 
	$xyzz = md5("$jkd$sesiku");


					

	//update
	mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET jml_benar = '$jml_benar', ".
					"jml_salah = '$jml_salah', ".
					"postdate = '$today' ".
					"WHERE siswa_kd = '$sesiku' ".
					"AND jadwal_kd = '$jkd'");
					
					
					
					
	//hitung jumlah yg dikerjakan
	$qyuk = mysqli_query($koneksi, "SELECT kd FROM siswa_soal ".
							"WHERE siswa_kd = '$sesiku' ".
							"AND jadwal_kd = '$jkd' ".
							"AND jawab <> ''");
	$ryuk = mysqli_fetch_assoc($qyuk);
	$tyuk = mysqli_num_rows($qyuk);
	
	


	//update nilai
	mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET waktu_selesai = '$today', ".
					"jml_soal_dikerjakan = '$tyuk', ".
					"jml_benar = '$jml_benar', ".
					"jml_salah = '$jml_salah' ".
					"WHERE siswa_kd = '$sesiku' ".
					"AND jadwal_kd = '$jkd'");
				
	}	






//isi
$isi = ob_get_contents();
ob_end_clean();

require("../inc/niltpl.php");


//null-kan
exit();
?>