<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();

//ambil nilai
require("../inc/config.php");
require("../inc/fungsi.php");
require("../inc/koneksi.php");
require("../inc/cek/siswa.php");
$tpl = LoadTpl("../template/siswa.html");


nocache;

//nilai
$filenya = "index.php";
$judul = "CALON PESERTA DIDIK BARU";
$judulku = "$judul  [$siswa_session]";




$jml_detik = "15000";







//ketahui detail siswa
$qyuk = mysqli_query($koneksi, "SELECT * FROM siswa ".
						"WHERE kd = '$kd61_session'");
$ryuk = mysqli_fetch_assoc($qyuk);
$yuk_noreg = balikin($ryuk['noreg']);
$yuk_nama = balikin($ryuk['nama']);
$yuk_jurusan = balikin($ryuk['hasil_jurusan']);





//isi *START
ob_start();


?>

              
                  <!-- Info boxes -->
      <div class="row">

        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-briefcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">NOMOR PENDAFTARAN</span>
              <span class="info-box-number"><?php echo $yuk_noreg;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        
                <!-- /.col -->
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">NAMA</span>
              <span class="info-box-number"><?php echo $yuk_nama;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->


                <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="glyphicon glyphicon-book"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">KEAHLIAN/JURUSAN YANG DIPILIH</span>
              <span class="info-box-number"><?php echo $yuk_jurusan;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->




        <!-- /.col -->
      </div>
      <!-- /.row -->








                  <!-- Info boxes -->
      <div class="row">

        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="glyphicon glyphicon-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SOAL SIAP KERJAKAN</span>
              <span class="info-box-number">
              	<?php
              	//detail soal
				$qku = mysqli_query($koneksi, "SELECT * FROM m_jadwal ".
												"ORDER BY mapel ASC");
				$rku = mysqli_fetch_assoc($qku);
				$tku = mysqli_num_rows($qku);
				
              	//jika ada
              	if (!empty($tku))
              		{
              		do
              			{
	              		//nilai
						$u_jkd = nosql($rku['kd']);
						$u_durasi = balikin($rku['durasi']);
						$u_mapel = balikin($rku['mapel']);
						$u_soal_jml = balikin($rku['soal_jml']);




						//yg dikerjakan...
						$qyuk = mysqli_query($koneksi, "SELECT * FROM siswa_soal ".
												"WHERE siswa_kd = '$kd61_session' ".
												"AND jadwal_kd = '$u_jkd' ".
												"AND jawab <> '' ".
												"ORDER BY postdate DESC");
						$ryuk = mysqli_fetch_assoc($qyuk);
						$yuk_postdate = balikin($ryuk['postdate']);
						$yuk_dikerjakan = mysqli_num_rows($qyuk);
						
						
						//jika telah dikerjakan
						if ($yuk_dikerjakan == $u_soal_jml)
							{
							echo '<p>
							<b>'.$u_mapel.'</b>
							</p>

							<p>
							<font color="red">
							<b>TELAH DIKERJAKAN PADA : '.$yuk_postdate.'</b>
							</font>
							</p>
							<hr>';								
							}
							
						else
							{	
			              	echo '<p>
							<b>'.$u_durasi.' Menit</b>.
							</p>
							
							<p>
							<b>'.$u_mapel.'</b>
							</p>
							
							<a href="soal.php?jkd='.$u_jkd.'" class="btn btn-block btn-danger">KERJAKAN ></a>
							<hr>
							<br>';
							}
						}
					while ($rku = mysqli_fetch_assoc($qku));
					}
				
				else
					{
					echo '<h3>
					<font color="red">BELUM ADA SOAL YANG BISA DIKERJAKAN..</font>
					</h3>';
					
					}
				?>
				              	
              	
              	
              	
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->






        <!-- /.col -->
      </div>
      <!-- /.row -->





<script>setTimeout("location.href='<?php echo $filenya;?>'", <?php echo $jml_detik;?>);</script>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../inc/niltpl.php");

//diskonek
exit();
?>