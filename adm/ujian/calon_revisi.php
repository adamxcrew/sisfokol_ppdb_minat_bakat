<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "calon_revisi.php";
$judul = "[CALON] Revisi Jawaban";
$judulku = "$judul";
$judulx = $judul;
$jkd = nosql($_REQUEST['jkd']);
$kd = nosql($_REQUEST['kd']);
$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}




//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika ke daftar
if ($_POST['btnDF'])
	{
	//re-direct
	$ke = "calon.php";
	xloc($ke);
	exit();
	}






//nek batal
if ($_POST['btnBTL'])
	{
	//nilai
	$jkd = nosql($_POST['jkd']);

	//re-direct
	$ke = "$filenya?jkd=$jkd";
	xloc($ke);
	exit();
	}





//jika cari
if ($_POST['btnCARI'])
	{
	//nilai
	$jkd = nosql($_POST['jkd']);	
	$kunci = cegah($_POST['kunci']);


	//re-direct
	$ke = "$filenya?jkd=$jkd&kunci=$kunci";
	xloc($ke);
	exit();
	}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//jika revisi jawaban
if ($s == "revisi")
	{
	//nilai
	$jkd = nosql($_REQUEST['jkd']);
	$swkd = nosql($_REQUEST['swkd']);
	$kunci = cegah($_REQUEST['kunci']);
	
	
	//update
	mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET revisi = 'true', ".
					"revisi_postdate = '$today' ".
					"WHERE jadwal_kd = '$jkd' ".
					"AND siswa_kd = '$swkd'");
	



	//re-direct
	//exit
	xclose($koneksi);
	$ke = "$filenya?jkd=$jkd&kunci=$kunci";
	xloc($ke);
	exit();
	}




else
	{		
	//isi *START
	ob_start();
	
	
	//require
	require("../../template/js/jumpmenu.js");
	require("../../template/js/checkall.js");
	require("../../template/js/swap.js");
	?>
	
	  
	  <script>
	$(document).ready(function() {
	$('#table-responsive').dataTable( {
	"scrollX": true
	    } );
	} );
	  </script>
	  
	<?php
	//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//detail jkd jadwal
	$qku = mysqli_query($koneksi, "SELECT * FROM m_jadwal ".
					"WHERE kd = '$jkd'");
	$rku = mysqli_fetch_assoc($qku);
	$u_durasi = balikin($rku['durasi']);
	$u_mapel = balikin($rku['mapel']);
	$u_soal_jml = balikin($rku['soal_jml']);

	
	
	
	
	//ketahui jumlah siswa yg mengerjakan
	$qjos = mysqli_query($koneksi, "SELECT DISTINCT(siswa_kd) AS skd ".
							"FROM siswa_soal_nilai ".
							"WHERE jadwal_kd = '$jkd'");
	$tjos = mysqli_num_rows($qjos);
	
	echo '<form action="'.$filenya.'" method="post" name="formxx">
	
	<p>
	[<b>'.$u_durasi.' Menit</b>].
	</p>
	
	<p>
	Mapel : <b>'.$u_mapel.'</b>
	</p>

	
	
	<p>
	<input name="jkd" type="hidden" value="'.$jkd.'">
	<input name="btnDF" type="submit" value="LIHAT KEAHLIAN LAIN >" class="btn btn-danger">
	</p>
	<br>
	
	</form>
	
	
	
	
	
	
	
	
	<form action="'.$filenya.'" method="post" name="formxx">';
	

	//jika null
	if (empty($kunci))
		{
		$sqlcount = "SELECT * FROM siswa ".
						"ORDER BY round(noreg) ASC";
		}
		
	else
		{
		$sqlcount = "SELECT * FROM siswa ".
						"WHERE noreg LIKE '%$kunci%' ".
						"OR nama LIKE '%$kunci%' ".
						"ORDER BY round(noreg) ASC";
	}
		
	
	//query
	$p = new Pager();
	$start = $p->findStart($limit);
	
	$sqlresult = $sqlcount;
	
	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$target = "$filenya?jkd=$jkd";
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);
		
	
	echo '<hr>
	<p>
	<input name="kunci" type="text" value="'.$kunci2.'" size="20" class="btn btn-warning">
	<input name="btnCARI" type="submit" value="CARI" class="btn btn-danger">
	<input name="btnBTL" type="submit" value="RESET" class="btn btn-info">
	<input name="s" type="hidden" value="'.$s.'">
	<input name="jkd" type="hidden" value="'.$jkd.'">
	
	</p>
		
		
	
	
	<div class="table-responsive">          
	<table class="table" border="1">
	<thead>
	
	<tr valign="top" bgcolor="'.$warnaheader.'">
	<td width="50"><strong><font color="'.$warnatext.'">NOREG</font></strong></td>
	<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">DIKERJAKAN</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">BENAR</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">SALAH</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">NILAI</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">POSTDATE</font></strong></td>
	<td width="50"><strong><font color="'.$warnatext.'">REVISI JAWABAN</font></strong></td>
	</tr>
	</thead>
	<tbody>';
	
	if ($count != 0)
		{
		do 
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
			$nomer = $nomer + 1;
			$i_kd = nosql($data['kd']);
			$i_nis = balikin($data['noreg']);
			$i_nama = balikin($data['nama']);

			
				 
			
			
			//ambil dari table siswa masing - masing
			$tablenilai = "siswa_soal_nilai";
			$qmboh = mysqli_query($koneksi, "SELECT * FROM $tablenilai ".
									"WHERE siswa_kd = '$i_kd' ".
									"AND jadwal_kd = '$jkd'");
			$rmboh = mysqli_fetch_assoc($qmboh);
			$mboh_kd = nosql($rmboh['kd']);
			$mboh_benar = nosql($rmboh['jml_benar']);
			$mboh_salah = nosql($rmboh['jml_salah']);
			$mboh_dikerjakan = nosql($rmboh['jml_soal_dikerjakaan']);
			$mboh_postdate = balikin($rmboh['postdate']);
			$mboh_rev_postdate = balikin($rmboh['revisi_postdate']);
			
			
			
			//update...
			if (empty($mboh_dikerjakan))
				{
				$mboh_dikerjakan = $mboh_benar + $mboh_salah;
				
				//update
				mysqli_query($koneksi, "UPDATE $tablenilai SET jml_soal_dikerjakan ='$mboh_dikerjakan' ".
								"WHERE siswa_kd = '$i_kd' ".
								"AND kd = '$mboh_kd'");
				}
				
				
			
			
			//jika null
			if (empty($mboh_dikerjakan))
				{
				$nilaiku = "-";
				}	
			else 
				{
				//nilainya..
				$nilaiku = ($mboh_benar / $mboh_dikerjakan) * 100;
				}
			
			
			
			
			
			//update
			mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET skor = '$nilaiku' ".
							"WHERE jadwal_kd = '$jkd' ".
							"AND siswa_kd = '$i_kd'");
			
			
			
				  
			
			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>'.$i_nis.'</td>
			<td>'.$i_nama.'</td>
			<td>'.$mboh_dikerjakan.'</td>
			<td>'.$mboh_benar.'</td>
			<td>'.$mboh_salah.'</td>
			<td>'.$nilaiku.'</td>
			<td>'.$mboh_postdate.'</td>
			<td>
			'.$mboh_rev_postdate.'
			<a href="'.$filenya.'?s=revisi&jkd='.$jkd.'&swkd='.$i_kd.'&kunci='.$kunci.'" class="btn btn-danger">RESET</a>
			</td>
			</tr>';
			}
		while ($data = mysqli_fetch_assoc($result));
		}
	
	
	echo '</tbody>
	  </table>
	  </div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
	<td>
	<strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'
	<br>
	
	<input name="jml" type="hidden" value="'.$count.'">
	<input name="s" type="hidden" value="'.$s.'">
	<input name="kd" type="hidden" value="'.$kdx.'">
	<input name="page" type="hidden" value="'.$page.'">
	</td>
	</tr>
	</table>
	</form>';
	
	
	
	
	
	//isi
	$isi = ob_get_contents();
	ob_end_clean();
	
	require("../../inc/niltpl.php");
	}





//null-kan
xclose($koneksi);
exit();
?>