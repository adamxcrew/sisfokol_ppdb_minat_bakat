<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();


//ambil nilai
require("inc/config.php");
require("inc/fungsi.php");
require("inc/koneksi.php");




nocache;

//nilai
$filenya = "calon_pdf.php";
$judul = "Print Kartu Tes";
$judulku = $judul;
$ku_judul = $judulku;
$s = nosql($_REQUEST['s']);
$kdx = nosql($_REQUEST['ckd']);
$kd = nosql($_REQUEST['ckd']);




require_once("inc/class/dompdf/autoload.inc.php");

use Dompdf\Dompdf;
$dompdf = new Dompdf();





//isi *START
ob_start();





//profil
$qx = mysqli_query($koneksi, "SELECT * FROM siswa ".
					"WHERE kd = '$kdx'");
$rowx = mysqli_fetch_assoc($qx);
$tx = mysqli_num_rows($qx);
$e_noreg = balikin($rowx['noreg']);
$e_nama = balikin($rowx['nama']);
$e_kelamin = balikin($rowx['kelamin']);
$e_jurusan = balikin($rowx['hasil_jurusan']);
$e_postdate = balikin($rowx['postdate']);

$e_user = balikin($rowx['usernamex']);
$e_pass = balikin($rowx['passwordx2']);
$e_lahir_tmp = balikin($rowx['lahir_tmp']);
$e_lahir_tgl = balikin($rowx['lahir_tgl']);

		
//pecah tanggal
$tgl2_pecah = balikin($e_lahir_tgl);
$tgl2_pecahku = explode("-", $tgl2_pecah);
$tgl2_tgl = trim($tgl2_pecahku[2]);
$tgl2_bln = trim($tgl2_pecahku[1]);
$tgl2_thn = trim($tgl2_pecahku[0]);
$tgl2_bln2 = $arrbln1[$tgl2_bln];


$e_lahir_tgl = "$tgl2_tgl $tgl2_bln2 $tgl2_thn";








$e_sekolah_asal = balikin($rowx['sekolah_asal']);
$e_user = balikin($rowx['username']);
$e_passx2 = balikin($rowx['passwordx2']);




?>


<table cellpadding="1" border="1" cellspacing="0">
	<tr valign="top">
		<td width="350">


		<table cellpadding="1" border="0" cellspacing="0">
			<tr>
				<td width="50">
					<img src="img/logo.jpg" width="75">					
				</td>
				<td width="200">
					<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt"> 
						KARTU UJIAN PENERIMAAN PESERTA DIDIK BARU
					</font></font>
					<br>
					
					<font face="Sans, sans-serif"><font size="2" style="font-size: 14pt"> 
						<?php echo $sek_nama;?>
					</font></font>
				</td>
			</tr>
		</table>
		
		<hr>
		
		
		<table cellpadding="1" cellspacing="0">
			<tr valign="top">
				<td width="50">
					
					<img src="filebox/calon/<?php echo $kdx;?>/<?php echo $kdx;?>-1.jpg" width="100" height="140">
					
				</td>
				
				<td>
						
			<table cellpadding="1" cellspacing="0">
				<tr valign="top">
					<td width="100">
						NO.REG
						
					</td>
					<td width="200">
						<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
							<?php echo $e_noreg;?>
						</font></font>
					</td>
				</tr>
				
	
				<tr valign="top">
					<td>
						NAMA
						
					</td>
					<td>
						<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
							<b><?php echo $e_nama;?></b>
						</font></font>
					</td>
				</tr>
	
				<tr valign="top">
					<td>
						JENIS KELAMIN
						
					</td>
					<td>
						<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
							<b><?php echo $e_kelamin;?></b>
						</font></font>
					</td>
				</tr>
				
				<tr valign="top">
					<td>
						TTL
						
					</td>
					<td>
						<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
							<b><?php echo $e_lahir_tmp;?>, <?php echo $e_lahir_tgl;?></b>
						</font></font>
					</td>
				</tr>
				
				<tr valign="top">
					<td>
						ASAL SEKOLAH
						
					</td>
					<td>
						<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
							<b><?php echo $e_sekolah_asal;?>
						</font></font>
					</td>
				</tr>
				
			</table>
			
			
			
			</td>
			
			</tr>
		</table>
		
		<hr>
		
		<table cellpadding="1" cellspacing="0">
			
			<tr valign="top">
				<td>
					TANGGAL PENDAFTARAN
					
				</td>
				<td>
					<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
						<b><?php echo $e_postdate;?></b>
					</font></font>
				</td>
			</tr>

			<tr valign="top">
				<td>
					KEAHLIAN YANG DIPILIH
					
				</td>
				<td>
					<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
						<b><?php echo $e_jurusan;?></b>
					</font></font>
				</td>
			</tr>


			<tr valign="top">
				<td>
					USERNAME
					
				</td>
				<td>
					<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
						<b><?php echo $e_noreg;?></b>
					</font></font>
				</td>
			</tr>
			
			
			<tr valign="top">
				<td>
					PASSWORD
					
				</td>
				<td>
					<font face="Sans, sans-serif"><font size="2" style="font-size: 10pt">: 
						<b><?php echo $e_passx2;?></b>
					</font></font>
				</td>
			</tr>
			
		</table>
		


		</td>
	</tr>	
</table>


<?php
//isi
$isi = ob_get_contents();
ob_end_clean();



//echo $isi;





$dompdf->loadHtml($isi);

// Setting ukuran dan orientasi kertas
$dompdf->setPaper('A4', 'potrait');
// Rendering dari HTML Ke PDF
$dompdf->render();


$pdf = $dompdf->output();

ob_end_clean();

// Melakukan output file Pdf
//$dompdf->stream('raport-$nis-$ku_nama2.pdf');
$dompdf->stream('calon-'.$e_noreg.'.pdf');
?>